﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Reloj2
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        const string saveFile = ".\\saveFile.bin";
        private AlarmaPersist alarma = new AlarmaPersist();
        private int seconds = 0;
        private int minutes = 0;
        private int hours = 0;

DispatcherTimer alarm = new DispatcherTimer();

        public MainWindow()
        {
            InitializeComponent();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(0);
            timer.Tick += Timer_Tick;
            timer.Start();
        }
        void Timer_Tick(object sender, EventArgs e)
        {
            lblTime.Content = DateTime.Now.ToLongTimeString();
        }


        private void Quit(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }
        private void Autor(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Sergi González Davant");
        }

        private void Alarm_Click_Active(object sender, RoutedEventArgs e)
        {
           if (active.IsChecked)
            {
                disable.IsChecked = false;
            }
            else
            {
                active.IsChecked = false;                
            }
        }

        private void Alarm_Click_Disable(object sender, RoutedEventArgs e)
        {
            if (disable.IsChecked)
            {
                active.IsChecked = false;
            }
            else
            {
                disable.IsChecked = false;
            }
        }

        private void showAlarma(object sender, EventArgs e)
        {
            MessageBox.Show("Alarma");
            alarm.Stop();
            disable.IsChecked = true;
        }

        private void alarmaCreate(object sender, RoutedEventArgs e)
        {
            if (active.IsChecked == true)
            {
                // Comprobar si son int
                if(comprobarInts(sec.Text, min.Text, hour.Text)) { 
                    seconds = int.Parse(sec.Text);
                    minutes = int.Parse(min.Text);
                    hours = int.Parse(hour.Text);
                    alarm.Tick += new EventHandler(showAlarma);
                    alarm.Interval = new TimeSpan(hours, minutes, seconds);
                    alarm.Start();
                }
                else
                {
                    MessageBox.Show("Solo introducir números positivos");
                }
            }

        }

        private bool comprobarInts(string text1, string text2, string text3)
        {
            bool boolean = true;
            if (!Int32.TryParse(text1, out seconds) || (!Int32.TryParse(text2, out minutes) || (!Int32.TryParse(text3, out hours))))
            {
                boolean = false;
            }else if(Int32.Parse(text1)<0 || Int32.Parse(text2)<0 || Int32.Parse(text3) < 0)
            {
                boolean = false;
            }
            return boolean;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (comprobarInts(sec.Text, min.Text, hour.Text))
            {
                Stream TestFileStream = File.Create(saveFile);
                BinaryFormatter serializer = new BinaryFormatter();
                alarma.Hora = int.Parse(hour.Text);
                alarma.Min = int.Parse(min.Text);
                alarma.Sec = int.Parse(sec.Text);
                alarma.Activa = active.IsChecked;
                serializer.Serialize(TestFileStream, alarma);
            }   
            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (File.Exists(saveFile)){
                Stream TestFileStream = File.OpenRead(saveFile);
                BinaryFormatter deserializer = new BinaryFormatter();
                alarma = (AlarmaPersist) deserializer.Deserialize(TestFileStream);
                seconds = this.alarma.Sec;
                minutes = this.alarma.Min;
                hours = this.alarma.Hora;
                active.IsChecked = this.alarma.Activa;
                TestFileStream.Close();
            }

            sec.Text = Convert.ToString(alarma.Sec);
            min.Text = Convert.ToString(alarma.Min);
            hour.Text = Convert.ToString(alarma.Hora);
            if (active.IsChecked == true)
            {
                seconds = int.Parse(sec.Text);
                minutes = int.Parse(min.Text);
                hours = int.Parse(hour.Text);
                alarm.Tick += new EventHandler(showAlarma);
                alarm.Interval = new TimeSpan(hours, minutes, seconds);

                alarm.Start();
            }
        }

        private void newClock(object sender, RoutedEventArgs e)
        {
            secClock secClock = new secClock();
            addClock.IsEnabled = false;
            secClock.Show();
        }

    }
}
