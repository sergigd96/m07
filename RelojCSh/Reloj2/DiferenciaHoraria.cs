﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reloj2
{
    class DiferenciaHoraria
    {
        private int hora;
        private String pais;

        public int Hora { get => hora; set => hora = value; }
        public string Pais { get => pais; set => pais = value; }

        public DiferenciaHoraria(int hora, string pais)
        {
            this.hora = hora;
            this.pais = pais;
        }

        public DiferenciaHoraria()
        {
        }

    }

    



}
