﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Reloj2
{
    /// <summary>
    /// Lógica de interacción para Window1.xaml
    /// </summary>
    public partial class secClock : Window
    {

        public secClock()
        {
            InitializeComponent();

        }

        Window1 w1 = new Window1();
        DiferenciaHoraria diferencia = null;
        private int varHora;

        private void btnDialogOk_Click(object sender, RoutedEventArgs e)
        {
            if (comprobarInts(hora.Text))
            {
                diferencia = new DiferenciaHoraria(Int32.Parse(hora.Text), pais.Text);
                w1.namePais.Content = diferencia.Pais;
                varHora = diferencia.Hora;
                genClock();
                Close();
            }
            else
            {
                MessageBox.Show("Solo introducir números");
            }
        }


        private void genClock()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(0);
            timer.Tick += Timer_Tick;
            timer.Start();
        }
        void Timer_Tick(object sender, EventArgs e)
        {
            w1.reloj2.Content = DateTime.Now.AddHours(varHora).ToLongTimeString();
            w1.Show();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            // Comprobar si se ha escrito algo
            if (diferencia == null) {
                //Si no se ha escrito nada, habilitar el item para añadir segundo reloj
                foreach (Window window in Application.Current.Windows)
                {
                    if (window.GetType() == typeof(MainWindow))
                    {
                        (window as MainWindow).addClock.IsEnabled = true;
                    }
                }
            }
        }

        private bool comprobarInts(string text1)
        {
            bool boolean = true;
            if (!Int32.TryParse(text1, out varHora ))
            {
                boolean = false;
            }
            return boolean;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
        
}