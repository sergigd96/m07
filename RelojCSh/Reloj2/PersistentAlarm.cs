﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reloj2
{
    [Serializable]
    public class AlarmaPersist
    {
        private int sec;
        private int min;
        private int hora;
        private bool activa;

        public AlarmaPersist()
        {
            this.sec = 0;
            this.min = 0;
            this.hora = 0;
            activa = false;
        }

        public AlarmaPersist(int sec, int min, int hora, bool activa)
        {
            this.sec = sec;
            this.min = min;
            this.hora = hora;
            this.activa = activa;
        }

        public int Sec { get => sec; set => sec = value; }
        public int Min { get => min; set => min = value; }
        public int Hora { get => hora; set => hora = value; }
        public bool Activa { get => activa; set => activa = value; }
    }
}